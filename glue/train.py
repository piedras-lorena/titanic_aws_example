from explainerdashboard.datasets import titanic_survive
from io import BytesIO
from sklearn.ensemble import RandomForestClassifier
import tempfile
import boto3

X_train, y_train, X_test, y_test = titanic_survive()
model = RandomForestClassifier(n_estimators=50, max_depth=5)
model.fit(X_train, y_train)

s3uri='s3://lorena-piedras-personal-projects/titanic_classification/random_forest_classification.pkl'

def write_bytes(s3uri,content):
    parts = s3uri.split('/')
    s3_bucket = parts[2]
    s3_key = '/'.join(parts[3:])
    content.seek(0)
    boto3.client("s3").upload_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=content)

def write_joblib(s3uri,obj):
    import joblib
    with BytesIO() as f:
        joblib.dump(obj, f)
        write_bytes(s3uri,f)


write_joblib(
    s3uri,
    model
)