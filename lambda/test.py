from titanic_prediction import lambda_handler
import json

test_event = {
    'body': json.dumps(
        {
        'Fare': 7.25,
        'Age': 22.0,
        'PassengerClass': 3.0,
        'No_of_siblings_plus_spouses_on_board': 1.0,
        'No_of_parents_plus_children_on_board': 0.0,
        'Sex_female': 0.0,
        'Sex_male': 1.0,
        'Sex_nan': 0.0,
        'Deck_A': 0.0,
        'Deck_B': 0.0,
        'Deck_C': 0.0,
        'Deck_D': 0.0,
        'Deck_E': 0.0,
        'Deck_F': 0.0,
        'Deck_G': 0.0,
        'Deck_T': 0.0,
        'Deck_Unkown': 1.0,
        'Embarked_Cherbourg': 0.0,
        'Embarked_Queenstown': 0.0,
        'Embarked_Southampton': 1.0,
        'Embarked_Unknown': 0.0}
    )
}

lambda_handler(test_event, None)