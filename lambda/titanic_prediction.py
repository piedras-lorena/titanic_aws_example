from io import BytesIO
import json
import boto3
import numpy as np
x_values = [
    'Fare', 'Age', 'PassengerClass', 'No_of_siblings_plus_spouses_on_board',
    'No_of_parents_plus_children_on_board', 'Sex_female', 'Sex_male',
    'Sex_nan', 'Deck_A', 'Deck_B', 'Deck_C', 'Deck_D', 'Deck_E', 'Deck_F',
    'Deck_G', 'Deck_T', 'Deck_Unkown', 'Embarked_Cherbourg',
    'Embarked_Queenstown', 'Embarked_Southampton', 'Embarked_Unknown']

def read_joblib(s3uri):
    import joblib
    with read_bytes(s3uri) as b:
        content = joblib.load(b)
    return content

def read_bytes(s3uri):
    parts = s3uri.split('/')
    s3_bucket = parts[2]
    s3_key = '/'.join(parts[3:])
    f = BytesIO()
    boto3.client("s3").download_fileobj(Bucket=s3_bucket, Key=s3_key, Fileobj=f)
    f.seek(0)
    return f
s3uri='s3://lorena-piedras-personal-projects/titanic_classification/random_forest_classification.pkl'
model = read_joblib(s3uri)

def lambda_handler(event, context):
    try:
        json_object = json.loads(event['body'])
        features = []
        for value in x_values:
            if not value in json_object:
                error = f'Missing feature: {value}'
                response = {
                    'statusCode': 400,
                    "body": json.dumps({"errors": error})
                }
                return response
            else:
                features.append(json_object[value])
        features_np = np.asarray(features)
        prediction = model.predict(features_np.reshape(1, -1))
        prediction_dict = json.dumps({'prediction': str(prediction[0])})
        response = {
            'statusCode': 200,
            'body': prediction_dict
        }
        return response
    except Exception as e:
        error = f'Unknown error: {e}'
        response = {
            'statusCode': 400,
            "body": json.dumps({"errors": error})
            }
        return response
    
